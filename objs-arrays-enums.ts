// const person: {
//     name: string;
//     age: number;
//     hobbies: string[];
//     role: [number, string];
// } = {
// const person = {
//     name: 'ali',
//     age: 30,
//     hobbies: ['Sports', 'Cooking'],
//     role: [2, 'author']
// }

enum Role {Admin=2,Reader=1,Writer=3};
// enum Role {Admin,Reader,Writer};

const person = {
    name: 'ali',
    age: 30,
    hobbies: ['Sports', 'Cooking'],
    role: 2
    // role: Role.Admin
}



// person.role.push('admin'); // typescript cant recognize push method
// person.role[1] = 10; // throw error because tuple has fixed length

let favoriteActivities: string[]; // array of strings
// favoriteActivities= 'Sports'; throw error 
// favoriteActivities = ['Sports', 1]; throw error for solve you should use type any[]
favoriteActivities = ['Sports', 'Cook']

console.log(person.name);

for (const hobby of person.hobbies) {
    console.log(hobby); // you can use multiple string methods because Typescript auto recognize the type of item in array
}

if(person.role==Role.Admin){
    console.log("is admin...");
}